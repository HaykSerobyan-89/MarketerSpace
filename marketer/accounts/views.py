import datetime
from django.contrib.sites.models import Site
import time_uuid
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from .mixins import PermissionPolicyMixin
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework_simplejwt.views import TokenObtainPairView
from .permissions import IsOrgSuperUser, IsOrgAdminUser, IsOrgUser
from .serializers import UserSerializer, MyTokenObtainPairSerializer, InvitationSerializer, RefreshTokenSerializer
from .models import User, Invitation
from rest_framework import viewsets, status
from .models import Organization
from .serializers import OrganizationSerializer
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from django.core.mail import send_mail


class UsersViewSet(PermissionPolicyMixin, viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.none()
    permission_classes_per_method = {
        "retrieve": [IsAuthenticated],
        "create": [IsOrgAdminUser | IsOrgSuperUser | IsAdminUser],
        "list": [IsAuthenticated],
        "update": [IsAuthenticated],
        "partial_update": [IsOrgSuperUser | IsOrgAdminUser | IsOrgUser | IsAdminUser],
        "destroy": [IsOrgUser | IsOrgAdminUser | IsOrgSuperUser | IsAdminUser],
    }

    def get_queryset(self):

        if self.request.user.is_authenticated:
            if self.request.user.is_superuser:
                return User.objects.all()
            elif self.request.user.is_admin:
                return User.objects.filter(organization=self.request.user.organization)
            else:
                return User.objects.filter(id=self.request.user.id)

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class OrganizationViewSet(PermissionPolicyMixin, viewsets.ModelViewSet):
    serializer_class = OrganizationSerializer
    queryset = Organization.objects.none()
    permission_classes_per_method = {
        "retrieve": [IsAuthenticated],
        "create": [IsOrgSuperUser | IsAdminUser],
        "list": [IsAuthenticated],
        "update": [IsAuthenticated],
        "partial_update": [IsAuthenticated],
        "destroy": [IsOrgAdminUser | IsOrgSuperUser | IsAdminUser],
    }

    def get_queryset(self):
        if self.request.user.is_authenticated:
            if self.request.user.is_superuser:
                return Organization.objects.all()
            else:
                return Organization.objects.filter(id=self.request.user.organization.id)


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class InvitationCreateView(CreateAPIView):
    serializer_class = InvitationSerializer
    queryset = Invitation.objects.all()
    permission_classes_per_method = {
        "create": [IsOrgAdminUser | IsOrgSuperUser | IsAdminUser],
    }

    # def pre_save(self, obj):
    #     obj.sender = self.request.user

    def create(self, request, *args, **kwargs):
        sender = self.request.user
        role = request.data.get('status')
        token = time_uuid.TimeUUID.with_utcnow()
        data = {
            'sender': sender.email,
            'organization': sender.organization.pk,
            'receiver': request.data.get('receiver', ''),
            'status': request.data.get('status', ''),
            'token': token
        }
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        # console send_email
        subject = f'Invitation from {sender.first_name} {sender.last_name}'
        message = f'{sender.first_name} {sender.last_name} ({sender.email}) has invited you to join ' \
                  f'{sender.organization.name} organization:\n' \
                  f'Accept http://{Site.objects.get_current()}/accounts/invited-user/{token}/?{role}=True'

        send_mail(
            subject,
            message,
            sender.email,
            [request.data.get('receiver')],
            fail_silently=False,
        )

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class InvitedUserRegisterView(CreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.none()

    def create(self, request, *args, **kwargs):
        token = kwargs.get('uuid')
        uuid_create_time = datetime.datetime.fromtimestamp((token.time - 0x01b21dd213814000) * 100 / 1e9)
        uuid_expire_time = uuid_create_time + datetime.timedelta(days=1)
        if uuid_expire_time > datetime.datetime.now():

            try:
                invite_info = Invitation.objects.get(token=token)
            except Invitation.DoesNotExist:
                return Response({"detail": "No invite found with this token"}, status=status.HTTP_400_BAD_REQUEST)

            data = {
                'email': invite_info.receiver,
                'organization': invite_info.organization.pk,
                'is_admin': request.GET.get('is_admin', False),
                'is_superuser': request.GET.get('is_superuser', False),
                'first_name': request.POST.get('first_name', ''),
                'last_name': request.POST.get('first_name', ''),
                'profile_picture': request.POST.get('profile_picture'),
                'country': request.POST.get('country'),
                'password': request.POST.get('password')
            }
            serializer = self.get_serializer(data=data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class LogoutView(APIView):
    serializer_class = RefreshTokenSerializer
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(
        responses={205: 'Reset Content', 400: 'Bad Request'},
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={'refresh_token': openapi.Schema(type=openapi.TYPE_STRING,
                                                                                description='JWT Refresh token')}), )
    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()
            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as err:
            return Response(err.args, status=status.HTTP_400_BAD_REQUEST)
