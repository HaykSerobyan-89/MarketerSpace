from django.test import SimpleTestCase
from django.urls import reverse, resolve
from accounts.views import MyTokenObtainPairView, LogoutView, InvitationCreateView
from rest_framework_simplejwt.views import TokenVerifyView, TokenRefreshView
from marketer.urls import schema_view
import marketer.urls


class ApiResolveTestCase(SimpleTestCase):

    def test_jwt_login_is_resolved(self):
        url = reverse('token_obtain_pair')
        self.assertEquals('/login/', url)
        self.assertEquals(resolve(url).func.view_class, MyTokenObtainPairView)

    def test_jwt_login_refresh_is_resolved(self):
        url = reverse('token_refresh')
        self.assertEquals('/login/refresh/', url)
        self.assertEquals(resolve(url).func.view_class, TokenRefreshView)

    def test_jwt_login_verify_is_resolved(self):
        url = reverse('token_verify')
        self.assertEquals('/login/verify/', url)
        self.assertEquals(resolve(url).func.view_class, TokenVerifyView)

    def test_jwt_logout_is_resolved(self):
        url = reverse('token_blacklist')
        self.assertEquals('/logout/', url)
        self.assertEquals(resolve(url).func.view_class, LogoutView)

    def test_debug_toolbar(self):
        if marketer.urls.Dev.DEBUG:
            self.assertTrue(len(marketer.urls.urlpatterns) == 11)

    def test_invitation_create_is_resolved(self):
        url = reverse('invitations')
        self.assertEquals('/accounts/invitations/', url)
        self.assertEquals(resolve(url).func.view_class, InvitationCreateView)

    def test_swagger_ui_is_resolved(self):
        url = reverse('schema-swagger-ui')
        self.assertEquals('/', url)
        self.assertEquals(resolve(url).func.view_class, schema_view)

    def test_swagger_redoc_is_resolved(self):
        url = reverse('schema-redoc')
        self.assertEquals('/redoc/', url)
        self.assertEquals(resolve(url).func.view_class, schema_view)
