import uuid
from django.test import TestCase
from accounts.models import Organization, User, Invitation
from accounts.serializers import UserSerializer, InvitationSerializer


class UserSerializerTestCase(TestCase):

    def test_user_serializer(self):
        organization = Organization.objects.create(name='Test', domain='test.com')
        data = {
            'email': 'test@test.com',
            'password': 'qwerty',
            'organization': organization.pk,
        }

        serializer = UserSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user = User.objects.get(email='test@test.com')
        self.assertEquals(user.email, data.get('email'))
        self.assertTrue(user.password != data.get('password'))


class InvitationSerializerTestCase(TestCase):

    def test_invitation_serializer(self):
        organization = Organization.objects.create(name='Test', domain='test.com')
        invitation_data = {
            'sender': 'sender@test.com',
            'organization': organization.pk,
            'receiver': 'receiver@test.com',
            'token': uuid.uuid1(),
            'status': 'is_admin',
        }
        invitation_serializer = InvitationSerializer(data=invitation_data)
        invitation_serializer.is_valid(raise_exception=True)
        invitation_serializer.save()

        invitation = Invitation.objects.get(receiver='receiver@test.com')
        self.assertEqual(invitation.__str__(), 'sender@test.com --> receiver@test.com')
