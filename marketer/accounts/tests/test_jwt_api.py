from django.test import TestCase
from rest_framework import status
from accounts.models import User, Organization


class JwtApiTestCase(TestCase):

    def test_login_api_when_user_inactive(self):
        user = User.objects.create_user(
            email='test@test.com',
            password='qwerty',
            organization=Organization.objects.create(name='Test', domain='test.com'),
            is_active=False
        )
        response = self.client.post('/login/', {'email': 'test@test.com', 'password': 'qwerty'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEquals(user.is_active, False)

    def test_login_api_when_user_active(self):
        user = User.objects.create_user(
            email='test@test.com',
            password='qwerty',
            organization=Organization.objects.create(name='Test', domain='test.com'),
            is_active=True
        )
        response = self.client.post('/login/', {'email': 'test@test.com', 'password': 'qwerty'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEquals(user.is_active, True)
        self.assertTrue('access' in response.data)
        self.assertTrue('refresh' in response.data)

    def test_jwt_login_refresh_api(self):
        User.objects.create_user(
            email='test@test.com',
            password='qwerty',
            organization=Organization.objects.create(name='Test', domain='test.com'),
            is_active=True
        )
        response = self.client.post('/login/', {'email': 'test@test.com', 'password': 'qwerty'}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
