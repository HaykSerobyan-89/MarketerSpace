from os.path import join
from accounts.models import User
from rest_framework import status
from marketer.settings.Dev import Dev
from accounts.views import UsersViewSet, OrganizationViewSet
from rest_framework.test import APIClient, APITestCase
from django.test import RequestFactory


class UsersViewSetTest(APITestCase):
    test_fixtures = [
        'organizations',
        'users'
    ]
    test_fixtures_list = []
    path_to_fixtures = join(str(Dev.BASE_DIR), '../accounts/fixtures/')
    for test_fixture in test_fixtures:
        test_fixtures_list.append(path_to_fixtures + '{}.json'.format(test_fixture))
    fixtures = test_fixtures_list

    def setUp(self):
        self.password = 'qwerty'
        self.factory = RequestFactory()
        self.client = APIClient()

        # Organization superuser login
        response = self.client.post('/login/', {'email': 'org_superuser@test.com', 'password': self.password})
        self.superuser = User.objects.get(email='org_superuser@test.com')
        self.superuser_header = {'HTTP_AUTHORIZATION': f'Bearer {response.data.get("access")}'}
        self.superuser_refresh_token = response.data.get("refresh")

        # Organization admin login
        response = self.client.post('/login/', {'email': 'org_admin@test.com', 'password': self.password})
        self.admin = User.objects.get(email='org_admin@test.com')
        self.admin_header = {'HTTP_AUTHORIZATION': f'Bearer {response.data.get("access")}'}

        # Organization user login
        response = self.client.post('/login/', {'email': 'org_user@test.com', 'password': self.password})
        self.user = User.objects.get(email='org_user@test.com')
        self.user_header = {'HTTP_AUTHORIZATION': f'Bearer {response.data.get("access")}'}

    def test_users_view_set_get(self):
        self.setUp()

        # Get users queryset with anonymous user
        request = self.factory.get('/accounts/users/')
        response = UsersViewSet.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Get users queryset with superuser
        request = self.factory.get('/accounts/users/', **self.superuser_header)
        response = UsersViewSet.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)

        # Get users queryset with admin
        request = self.factory.get('/accounts/users/', **self.admin_header)
        response = UsersViewSet.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)

        # Get users queryset with user
        request = self.factory.get('/accounts/users/', **self.user_header)
        response = UsersViewSet.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0].get('email'), self.user.__str__())

    def test_users_view_set_post(self):
        self.setUp()

        # Create user without authentication
        response = self.client.post('/accounts/users/', data={})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Create user without data
        self.client.credentials(HTTP_AUTHORIZATION=self.superuser_header.get('HTTP_AUTHORIZATION'))
        response = self.client.post('/accounts/users/', data={})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Create user with user without a specific role
        self.client.credentials(HTTP_AUTHORIZATION=self.user_header.get('HTTP_AUTHORIZATION'))
        response = self.client.post('/accounts/users/', data={})
        self.assertTrue('You do not have permission to perform this action.' == response.data.get('detail'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_view_set_partial_update(self):
        self.setUp()

        # Change user field with organization user
        self.client.credentials(HTTP_AUTHORIZATION=self.user_header.get('HTTP_AUTHORIZATION'))
        response = self.client.patch('/accounts/users/4/', data={'is_active': False})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_view_set_not_allowed_method(self):
        self.setUp()

        self.client.credentials(HTTP_AUTHORIZATION=self.user_header.get('HTTP_AUTHORIZATION'))
        response = self.client.patch('/accounts/users/', data={'is_active': False})
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_user_destroy(self):
        self.setUp()

        # delete self
        self.client.credentials(HTTP_AUTHORIZATION=self.user_header.get('HTTP_AUTHORIZATION'))
        response = self.client.delete('/accounts/users/4/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # delete another user with user
        self.client.credentials(HTTP_AUTHORIZATION=self.user_header.get('HTTP_AUTHORIZATION'))
        response = self.client.delete('/accounts/users/3/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # delete another user with superuser
        self.client.credentials(HTTP_AUTHORIZATION=self.superuser_header.get('HTTP_AUTHORIZATION'))
        response = self.client.delete('/accounts/users/3/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_organizations_view_set_get(self):
        self.setUp()

        # Get organizations queryset with anonymous user
        request = self.factory.get('/accounts/organizations/')
        response = OrganizationViewSet.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Get organizations queryset with superuser
        request = self.factory.get('/accounts/organizations/', **self.superuser_header)
        response = OrganizationViewSet.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        # Get organizations queryset with admin
        request = self.factory.get('/accounts/organizations/', **self.admin_header)
        response = OrganizationViewSet.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        # Get organizations queryset with user
        request = self.factory.get('/accounts/users/', **self.user_header)
        response = OrganizationViewSet.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_organizations_view_set_post(self):
        self.setUp()

        # Create user without authentication
        response = self.client.post('/accounts/organizations/', data={})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Create user without data
        self.client.credentials(HTTP_AUTHORIZATION=self.superuser_header.get('HTTP_AUTHORIZATION'))
        response = self.client.post('/accounts/organizations/', data={})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Create user with user without a specific role
        self.client.credentials(HTTP_AUTHORIZATION=self.user_header.get('HTTP_AUTHORIZATION'))
        response = self.client.post('/accounts/organizations/', data={})
        self.assertTrue('You do not have permission to perform this action.' == response.data.get('detail'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_organization_by_id(self):
        self.setUp()

        # Get organization with user without role
        self.client.credentials(HTTP_AUTHORIZATION=self.user_header.get('HTTP_AUTHORIZATION'))
        response = self.client.get('/accounts/organizations/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('name'), self.user.organization.name)
        self.assertEqual('gohacktech.com - HackTech', self.user.organization.__str__())

        # Get other organization with user without role
        self.client.credentials(HTTP_AUTHORIZATION=self.user_header.get('HTTP_AUTHORIZATION'))
        response = self.client.get('/accounts/organizations/2/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Get organization with admin user
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_header.get('HTTP_AUTHORIZATION'))
        response = self.client.get('/accounts/organizations/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('name'), self.user.organization.name)

        # Get other organization with admin user
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_header.get('HTTP_AUTHORIZATION'))
        response = self.client.get('/accounts/organizations/2/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Get organization with superuser
        self.client.credentials(HTTP_AUTHORIZATION=self.superuser_header.get('HTTP_AUTHORIZATION'))
        response = self.client.get('/accounts/organizations/2/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data.get('name') != self.user.organization.name)

    def test_invitation_create(self):
        self.setUp()

        # create invitation with empty data
        self.client.credentials(HTTP_AUTHORIZATION=self.admin_header.get('HTTP_AUTHORIZATION'))
        response = self.client.post('/accounts/invitations/', data={})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # create invitation with superuser
        self.client.credentials(HTTP_AUTHORIZATION=self.superuser_header.get('HTTP_AUTHORIZATION'))
        response = self.client.post('/accounts/invitations/', data={'receiver': 'invited_user@test.com',
                                                                    'status': 'is_admin'})
        self.assertEqual(response.data.get('sender'), self.superuser.email)
        self.assertEqual(response.data.get('organization'), self.superuser.organization.pk)
        self.assertEqual(response.data.get('status'), 'is_admin')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        token_uuid = response.data.get('token')

        # User register with invitation link
        self.client.credentials()
        response = self.client.post(f'/accounts/invited-user/{token_uuid}/?is_admin=True',
                                    data={'password': 'qwerty'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # User register with incorrect token
        token_uuid = token_uuid[:-1] + '1'
        self.client.credentials()
        response = self.client.post(f'/accounts/invited-user/{token_uuid}/?is_admin=True',
                                    data={'password': 'qwerty'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_logout(self):
        # Superuser logout with incorrect data
        self.client.credentials(HTTP_AUTHORIZATION=self.superuser_header.get('HTTP_AUTHORIZATION'))
        response = self.client.post('/logout/', data={})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Superuser logout with correct data
        self.client.credentials(HTTP_AUTHORIZATION=self.superuser_header.get('HTTP_AUTHORIZATION'))
        response = self.client.post('/logout/', data={'refresh_token': self.superuser_refresh_token})
        self.assertEqual(response.status_code, status.HTTP_205_RESET_CONTENT)

    def test_user_password_change(self):
        self.client.credentials(HTTP_AUTHORIZATION=self.superuser_header.get('HTTP_AUTHORIZATION'))
        response = self.client.patch('/accounts/users/3/', data={'password': 'qwerty'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
