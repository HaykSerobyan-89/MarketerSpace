from rest_framework.exceptions import ValidationError
from rest_framework.test import APITestCase
from accounts.models import User, Organization
from accounts.serializers import UserSerializer


class UserManagerTestCase(APITestCase):

    def test_create_superuser_is_staff_false(self):
        try:
            User.objects.create_superuser(
                email='test@test.com',
                password='qwerty',
                organization=Organization.objects.create(name='Test', domain='test.com'),
                is_staff=False
            )
        except ValueError as err:
            self.assertRaisesMessage(err.args, 'Superuser must have is_staff=True')

    def test_create_superuser_is_superuser_false(self):
        try:
            User.objects.create_superuser(
                email='test@test.com',
                password='qwerty',
                organization=Organization.objects.create(name='Test', domain='test.com'),
                is_superuser=False
            )
        except ValueError as err:
            self.assertRaisesMessage(err.args, 'Superuser must have is_superuser=True')

    def test_create_superuser_is_admin_false(self):
        try:
            User.objects.create_superuser(
                email='test@test.com',
                password='qwerty',
                organization=Organization.objects.create(name='Test', domain='test.com'),
                is_admin=False
            )
        except ValueError as err:
            self.assertRaisesMessage(err.args, 'Superuser must have is_admin=True')

    def test_create_superuser(self):
        organization = Organization.objects.create(name='Test', domain='test.com')
        superuser = User.objects.create_superuser(
            email='test@test.com',
            password='qwerty',
            organization=organization,
            organization_id=organization.id,
        )
        self.assertIsInstance(superuser, User)
        self.assertTrue(superuser.is_staff is True)
        self.assertTrue(superuser.is_superuser is True)
        self.assertTrue(superuser.is_admin is True)

    def test_create_user_without_email(self):
        try:
            user_data = {
                'password': 'qwerty',
                'organization': Organization.objects.create(name='Test', domain='test.com'),
            }
            serializer = UserSerializer(data=user_data)
            serializer.is_valid(raise_exception=True)

        except ValidationError as err:
            self.assertRaisesMessage(err.detail.get('email'),
                                     "[ErrorDetail(string='This field may not be blank.', code='blank')]")

    def test_create_user_without_organization(self):
        try:
            user_data = {
                'email': 'test@test.com',
                'password': 'qwerty',
            }
            serializer = UserSerializer(data=user_data)
            serializer.is_valid(raise_exception=True)

        except ValidationError as err:
            self.assertRaisesMessage(err.detail.get('organization'),
                                     "[ErrorDetail(string='This field may not be blank.', code='required')]")
