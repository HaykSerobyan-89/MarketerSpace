from unittest import TestCase
from django.core.handlers.asgi import ASGIHandler
from django.core.handlers.wsgi import WSGIHandler
from marketer.wsgi import get_wsgi_application
from marketer.asgi import get_asgi_application
from marketer.settings.Prod import Prod


class WSGISettingsTestCase(TestCase):
    def test_wsgi_application(self):
        app = get_wsgi_application()
        self.assertIsInstance(app, WSGIHandler)


class ASGISettingsTestCase(TestCase):
    def test_asgi_application(self):
        app = get_asgi_application()
        self.assertIsInstance(app, ASGIHandler)


class ProductionSettingsTestCase(TestCase):
    def test_production_debug(self):
        self.assertEquals(Prod.DEBUG, False)

    def test_production_allowed_hosts(self):
        self.assertEquals(Prod.ALLOWED_HOSTS, ['www.example.com'])
