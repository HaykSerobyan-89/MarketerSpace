# Generated by Django 4.1 on 2022-09-09 18:58

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(editable=False, primary_key=True, serialize=False)),
                ('domain', models.CharField(max_length=20, unique=True, verbose_name='Domain')),
                ('name', models.CharField(max_length=20, verbose_name='Name')),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Invitation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sender', models.EmailField(max_length=100)),
                ('receiver', models.EmailField(max_length=100)),
                ('token', models.UUIDField()),
                ('status', models.CharField(
                    choices=[('is_user', 'Org. User'), ('is_admin', 'Org. Admin'), ('is_superuser', 'Org. SuperUser')],
                    default='Org. User', max_length=100, verbose_name='Status')),
                ('organization',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='invitations',
                                   to='accounts.organization')),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('first_name', models.CharField(blank=True, max_length=150, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('is_staff', models.BooleanField(default=False,
                                                 help_text='Designates whether the user can log into this admin site.',
                                                 verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True,
                                                  help_text='Designates whether this user should be treated as active. '
                                                            'Unselect this instead of deleting accounts.',
                                                  verbose_name='active')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='Email')),
                ('country', models.CharField(blank=True, choices=[('', 'choose your country'), ('arm', 'Armenia'),
                                                                  ('rus', 'Russia'), ('eng', 'Greate Britain'),
                                                                  ('usa', 'United States'), ('fra', 'France')],
                                             max_length=10, null=True, verbose_name='Country')),
                ('profile_picture', models.ImageField(blank=True, null=True, upload_to='profile',
                                                      validators=[django.core.validators.validate_image_file_extension,
                                                                  django.core.validators.FileExtensionValidator(
                                                                      allowed_extensions=['jpeg', 'png', 'jpg'])])),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('is_superuser', models.BooleanField(default=False)),
                ('is_admin', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(blank=True,
                                                  help_text='The groups this user belongs to. A user will get all '
                                                            'permissions granted to each of their groups.',
                                                  related_name='user_set', related_query_name='user', to='auth.group',
                                                  verbose_name='groups')),
                ('organization',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='organization',
                                   to='accounts.organization')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.',
                                                            related_name='user_set', related_query_name='user',
                                                            to='auth.permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
        ),
    ]
