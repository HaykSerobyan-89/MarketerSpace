from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers
from .models import Organization, User, Invitation
from django.contrib.auth.hashers import make_password
from marketer.settings.Base import Base


class OrganizationSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(read_only=True, format=Base.DATETIME_FORMAT)

    class Meta:
        model = Organization
        fields = ('id', 'domain', 'name', 'created')


class UserSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(read_only=True, format=Base.DATETIME_FORMAT)
    modified = serializers.DateTimeField(read_only=True, format=Base.DATETIME_FORMAT)
    password = serializers.CharField(write_only=True,
                                     required=True,
                                     style={'input_type': 'password', 'placeholder': 'Password'})

    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data.get('password'))
        return super(UserSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        if 'password' in validated_data:
            validated_data['password'] = make_password(validated_data.get('password'))
        return super(UserSerializer, self).update(instance, validated_data)

    class Meta:
        model = User
        fields = ('id', 'email', 'organization', 'first_name', 'last_name',
                  'country', 'profile_picture', 'password', 'created', 'modified')


class InvitationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invitation
        fields = ('sender', 'organization', 'receiver', 'token', 'status')


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['email'] = user.email
        token['user.organization_name'] = user.organization.name
        token['user.first_name'] = user.first_name
        token['user.last_name'] = user.last_name

        return token


class RefreshTokenSerializer(serializers.Serializer):
    refresh_token = serializers.CharField()

    default_error_messages = {
        'bad_token': 'Token is invalid or expired'
    }
