from django.core.validators import FileExtensionValidator, validate_image_file_extension
from accounts.managers import UserManager
from marketer.settings.Base import Base
from django.db import models
from django.contrib.auth.models import AbstractUser


class Organization(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    domain = models.CharField('Domain', max_length=20, unique=True)
    name = models.CharField('Name', max_length=20, blank=False, null=False)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.domain} - {self.name}'


class User(AbstractUser):
    username = None
    date_joined = None
    email = models.EmailField('Email', unique=True)
    country = models.CharField('Country', max_length=10, choices=Base.COUNTRY_CHOICES, blank=True, null=True)
    profile_picture = models.ImageField(upload_to='profile',
                                        validators=[validate_image_file_extension,
                                                    FileExtensionValidator(
                                                        allowed_extensions=['jpeg', 'png', 'jpg'])],
                                        blank=True,
                                        null=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='organization')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    is_superuser = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['organization']

    def __str__(self):
        return f'{self.email}'


class Invitation(models.Model):
    organization = models.ForeignKey(Organization, related_name='invitations', on_delete=models.CASCADE)
    sender = models.EmailField(max_length=100)
    receiver = models.EmailField(max_length=100)
    token = models.UUIDField()
    status = models.CharField('Status', max_length=100, choices=Base.STATUS_CHOICES, default='Org. User')

    def __str__(self):
        return f'{self.sender} --> {self.receiver}'
