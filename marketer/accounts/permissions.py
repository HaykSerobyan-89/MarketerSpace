from rest_framework.permissions import BasePermission


class IsOrgSuperUser(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return bool(request.user and request.user.is_superuser)


class IsOrgAdminUser(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return bool(request.user and request.user.is_admin)


class IsOrgUser(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return bool(request.user and request.user.is_active)
