from django.urls import include, path
from rest_framework import routers
from .views import UsersViewSet, OrganizationViewSet, InvitationCreateView, InvitedUserRegisterView

router = routers.DefaultRouter()
router.register('users', UsersViewSet)
router.register('organizations', OrganizationViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('invitations/', InvitationCreateView.as_view(), name='invitations'),
    path('invited-user/<uuid:uuid>/', InvitedUserRegisterView.as_view(), name='invited_user'),
]
