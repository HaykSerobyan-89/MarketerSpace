from django.contrib import admin
from .models import ContactCsvFile, Contact, Campaign, Template

admin.site.register(ContactCsvFile)
admin.site.register(Contact)
admin.site.register(Campaign)
admin.site.register(Template)
