import csv

from django.db.utils import IntegrityError
from campaign.models import Contact, ContactCsvFile


def create_contacts_from_csv(csv_path, file_id):
    with open(csv_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count, not_created, created = 0, 0, 0

        for row in csv_reader:
            if line_count == 0:
                line_count += 1
                continue
            try:
                Contact.objects.create(
                    first_name=row[0],
                    last_name=row[1],
                    email=row[2],
                    company_name=row[3],
                    job_title=row[4],
                    contact_file=ContactCsvFile.objects.get(pk=file_id)
                )
                created += 1
                line_count += 1
            except IntegrityError:
                not_created += 1
                line_count += 1

    return line_count - 1, created, not_created
