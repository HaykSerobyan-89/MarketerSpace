from celery import shared_task
from datetime import datetime

from django.core.mail import send_mail

from campaign.models import Campaign, CampaignStatusChoices
from marketer.celery import app


@app.task
def test(arg):
    print(arg)


@shared_task()
def time_now():
    return datetime.now()


@shared_task()
def check_campaign_schedule_time():
    completed_campaigns = []
    for campaign in Campaign.objects.all():
        if campaign.status == CampaignStatusChoices.NotStarted and campaign.scheduled_time < datetime.now():
            campaign.started()
            contact_files = campaign.contact_files
            print(contact_files)
            # add send email function with template

    return completed_campaigns


@shared_task()
def send_users_creation_report(processed, created, not_created, mail):
    if processed == created:
        message = f"""Processed {processed} lines.
                      All {created} users successfully created."""
    else:
        message = f"""Processed {processed} lines.
                      Successfully created {created} users.
                      Not created, because already existed {not_created} users."""

    send_mail(
        subject="Contact creation report",
        message=message,
        from_email='support@gohacktech.com',
        recipient_list=[mail],
        fail_silently=False,
    )

    return "Report successfully sent"
