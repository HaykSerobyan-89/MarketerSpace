from django.urls import include, path
from rest_framework import routers

from campaign import views

router = routers.DefaultRouter()
router.register('templates', views.TemplateViewSet)
router.register('campaigns', views.CampaignViewSet)
router.register('contacts', views.ContactViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('campaigns/<int:pk>/files/', views.ContactsFileCsvViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('campaigns/<int:pk>/files/<int:file_id>/', views.ContactsFileCsvDestroy.as_view()),
]
