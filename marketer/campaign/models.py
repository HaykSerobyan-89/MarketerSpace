from django.core.validators import FileExtensionValidator
from django.db import models
from django_fsm import transition, FSMField

from accounts.models import User
from campaign.validators import validate_file_size


class CampaignStatusChoices(models.TextChoices):
    NotStarted = 'not_started', 'Not started'
    Started = 'started', 'Started'
    Paused = 'paused', 'Paused'
    Completed = 'completed', 'Completed'


class ContactCsvFile(models.Model):
    csv_path = models.FileField(upload_to='csv_files',
                                validators=[validate_file_size,
                                            FileExtensionValidator(allowed_extensions=['csv'])],
                                blank=True,
                                null=True)
    uploaded_at = models.IntegerField("Campaign id", blank=False, null=False)
    uploaded_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False)


class Contact(models.Model):
    first_name = models.CharField("Name", max_length=100, blank=True, null=True)
    last_name = models.CharField("Surname", max_length=100, blank=True, null=True)
    email = models.EmailField("Email", max_length=100, unique=True)
    company_name = models.CharField("Company name", max_length=100, blank=True, null=True)
    job_title = models.CharField("Job title", max_length=100, blank=True, null=True)
    contact_file = models.ForeignKey(ContactCsvFile, on_delete=models.CASCADE, blank=False,
                                     null=False)


class Template(models.Model):
    subject = models.CharField("Subject", max_length=50, blank=True, null=True)
    content = models.TextField("Content", blank=True, null=True)


class Campaign(models.Model):
    goal = models.CharField('Goal', max_length=20, blank=True, null=True)
    scheduled_time = models.DateTimeField("Scheduled time")
    status = FSMField('Status', max_length=26, default=CampaignStatusChoices.NotStarted,
                      choices=CampaignStatusChoices.choices, protected=True)
    contact_files = models.ManyToManyField(ContactCsvFile, blank=True)
    template = models.ForeignKey(Template, on_delete=models.CASCADE, blank=True, null=True)

    @transition(field=status,
                source=[CampaignStatusChoices.Paused, CampaignStatusChoices.NotStarted],
                target=CampaignStatusChoices.Started)
    def started(self):
        return "Started!"

    @transition(field=status, source=CampaignStatusChoices.Started,
                target=CampaignStatusChoices.Paused)
    def paused(self):
        return "Paused!"

    @transition(field=status, source=[CampaignStatusChoices.Started, CampaignStatusChoices.Paused],
                target=CampaignStatusChoices.Completed)
    def completed(self):
        return "Completed!"
