from rest_framework import serializers
from campaign.models import ContactCsvFile, Contact, Template, Campaign


class ContactsFileCsvSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactCsvFile
        fields = ('id', 'csv_path', 'uploaded_at', 'uploaded_by')


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ('id', 'first_name', 'last_name', 'email', 'company_name', 'job_title', 'contact_file')


class TemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Template
        fields = ('id', 'subject', 'content')


class CampaignSerializer(serializers.ModelSerializer):
    class Meta:
        model = Campaign
        fields = ('id', 'goal', 'scheduled_time', 'status', 'contact_files', 'template')
