import json

from django.core.serializers import serialize
from rest_framework import viewsets, status
from drf_fsm.mixins import FsmViewSetMixin
from rest_framework.generics import RetrieveDestroyAPIView
from rest_framework.response import Response

from campaign.serializers import CampaignSerializer, TemplateSerializer, \
    ContactsFileCsvSerializer, ContactSerializer
from campaign.models import Template, Campaign, Contact, ContactCsvFile, CampaignStatusChoices
from campaign.services import create_contacts_from_csv
from campaign.tasks import send_users_creation_report


class ContactsFileCsvViewSet(viewsets.ModelViewSet):
    serializer_class = ContactsFileCsvSerializer

    def get_queryset(self):
        campaign_id = self.kwargs.get('pk')
        return ContactCsvFile.objects.filter(uploaded_at=campaign_id)

    def create(self, request, *args, **kwargs):
        data = {
            'uploaded_at': self.kwargs.get('pk'),
            # will be changed to `self.request.user`
            'uploaded_by': 1,
            'csv_path': request.data.get('csv_path')
        }

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        # add file to campaign files list
        campaign = Campaign.objects.get(pk=self.kwargs.get('pk'))
        if campaign.status != CampaignStatusChoices.NotStarted:
            message = {
                "detail": "You can't upload file because campaign is started"
            }
            return Response(message, status=status.HTTP_400_BAD_REQUEST, headers=headers)
        campaign.contact_files.add(serializer.data.get('id'))

        # Read rows from csv and create contacts
        processed, created, not_created = create_contacts_from_csv(
            csv_path=f'marketer/media/csv_files/{data.get("csv_path")}',
            file_id=serializer.data.get('id'))

        # after testing change mail to 'self.request.user.email'
        send_users_creation_report.delay(processed, created, not_created,
                                         mail='hayk.serobyan.89@mail.ru')

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class ContactsFileCsvDestroy(RetrieveDestroyAPIView):
    serializer_class = ContactsFileCsvSerializer

    def get_queryset(self):
        return ContactCsvFile.objects.filter(pk=self.kwargs.get('file_id'))

    def get(self, request, *args, **kwargs):
        str_data = serialize('json', ContactCsvFile.objects.filter(pk=self.kwargs.get('file_id')))
        data = json.loads(str_data)
        return Response(data, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        ContactCsvFile.objects.get(pk=self.kwargs.get('file_id')).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ContactViewSet(viewsets.ModelViewSet):
    serializer_class = ContactSerializer
    queryset = Contact.objects.all()


class TemplateViewSet(viewsets.ModelViewSet):
    serializer_class = TemplateSerializer
    queryset = Template.objects.all()


class CampaignViewSet(FsmViewSetMixin, viewsets.ModelViewSet):
    serializer_class = CampaignSerializer
    queryset = Campaign.objects.all()
    fsm_fields = ['status']

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        # remove request.data.status from data
        data = {
            'goal': request.data.get('goal'),
            'scheduled_time': request.data.get('scheduled_time'),
            'contact_files': [int(el) for el in
                              [el for el in request.data._getlist('contact_files')]],
            'template': int(request.data.get('template')),
        }
        serializer = self.get_serializer(instance, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
