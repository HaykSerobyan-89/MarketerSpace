from rest_framework.exceptions import ValidationError


def validate_file_size(value):
    file_size = value.size

    if file_size > 2 * 1024 * 1024:
        raise ValidationError("You cannot upload file more than 2Mb")
    else:
        return value
