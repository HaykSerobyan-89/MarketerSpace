from marketer.settings.Base import Base
from pathlib import Path
import os


class Dev(Base):
    DEBUG = True

    ALLOWED_HOSTS = ['127.0.0.1', 'localhost', '0.0.0.0']

    # Build paths inside the project like this: BASE_DIR / 'subdir'.
    BASE_DIR = Path(__file__).resolve().parent.parent

    # Database
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.getenv('DB_NAME'),
            'USER': os.getenv('DB_USER'),
            'PASSWORD': os.getenv('DB_PASSWORD'),
            'HOST': os.getenv('DB_HOST'),
            'PORT': os.getenv('DB_PORT'),
        },
    }

    # Static files (CSS, JavaScript, Images)
    STATIC_URL = '/static/'
    STATIC_ROOT = os.path.join(BASE_DIR, "/static/")

    # Media files for user profile picture (Images)
    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

    SITE_ID = 1

    Base.INSTALLED_APPS += [
        'debug_toolbar',
    ]

    Base.INTERNAL_IPS = [
        "127.0.0.1",
    ]

    # CORS_ALLOWED_ORIGINS = [
    #     'http://localhost:3000',
    # ]

    CORS_ALLOW_ALL_ORIGINS = True
    CORS_ALLOW_CREDENTIALS = True

    Base.MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
        'debug_toolbar_force.middleware.ForceDebugToolbarMiddleware',
    ]
