from marketer.settings.Base import Base


class Prod(Base):

    DEBUG = False

    ALLOWED_HOSTS = ['www.example.com']
