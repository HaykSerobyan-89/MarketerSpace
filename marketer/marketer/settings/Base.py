import os
from datetime import timedelta

from configurations import Configuration
import environ


class Base(Configuration):
    env = environ.Env()
    environ.Env.read_env()

    SECRET_KEY = os.getenv('SECRET_KEY')

    # Application definition
    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.sites',

        # third party packages
        'corsheaders',
        'rest_framework',
        'rest_framework_simplejwt',
        'rest_framework_simplejwt.token_blacklist',
        'configurations',
        'drf_yasg',
        'django_celery_beat',

        # internal apps
        'accounts',
        'campaign',
    ]

    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ]

    ROOT_URLCONF = 'marketer.urls'

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]

    ASGI_APPLICATION = 'marketer.asgi.application'
    WSGI_APPLICATION = 'marketer.wsgi.application'

    # Password validation
    AUTH_PASSWORD_VALIDATORS = [
        {
            'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
        },
    ]

    # Simple JWT settings
    SIMPLE_JWT = {
        'ACCESS_TOKEN_LIFETIME': timedelta(minutes=60),
        'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
        'ROTATE_REFRESH_TOKENS': False,
        'BLACKLIST_AFTER_ROTATION': False,
        'UPDATE_LAST_LOGIN': False,

        'ALGORITHM': 'HS256',
        'SIGNING_KEY': SECRET_KEY,
        'VERIFYING_KEY': None,
        'AUDIENCE': None,
        'ISSUER': None,
        'JWK_URL': None,
        'LEEWAY': 0,

        'AUTH_HEADER_TYPES': ('Bearer',),
        'AUTH_HEADER_NAME': 'HTTP_AUTHORIZATION',
        'USER_ID_FIELD': 'id',
        'USER_ID_CLAIM': 'user_id',
        'USER_AUTHENTICATION_RULE': 'rest_framework_simplejwt.authentication.default_user_authentication_rule',

        'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
        'TOKEN_TYPE_CLAIM': 'token_type',
        'TOKEN_USER_CLASS': 'rest_framework_simplejwt.models.TokenUser',

        'JTI_CLAIM': 'jti',

        'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
        'SLIDING_TOKEN_LIFETIME': timedelta(minutes=5),
        'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),
    }

    # Simple JWT authentication
    REST_FRAMEWORK = {
        'DEFAULT_AUTHENTICATION_CLASSES': (
            'rest_framework_simplejwt.authentication.JWTAuthentication',
        ),
        'DEFAULT_PERMISSION_CLASSES': [
            'rest_framework.permissions.AllowAny',
        ],
        'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
    }

    # Internationalization
    LANGUAGE_CODE = 'en-us'

    TIME_ZONE = 'Asia/Yerevan'

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True

    # Default primary key field type
    DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

    # Custom user model
    AUTH_USER_MODEL = 'accounts.User'

    DATETIME_FORMAT = '%d/%m/%Y %H:%M:%S'

    # Country choices list for user
    COUNTRY_CHOICES = (
        ('', 'choose your country'),
        ('arm', 'Armenia'),
        ('rus', 'Russia'),
        ('eng', 'Greate Britain'),
        ('usa', 'United States'),
        ('fra', 'France'),
    )

    STATUS_CHOICES = (
        ('is_user', 'Org. User'),
        ('is_admin', 'Org. Admin'),
        ('is_superuser', 'Org. SuperUser'),
    )

    SWAGGER_SETTINGS = {
        'USE_SESSION_AUTH': False,

        'SECURITY_DEFINITIONS': {
            "Auth Token eg [Bearer (JWT) ]": {
                "type": "apiKey",
                "name": "Authorization",
                "in": "header"
            }
        }
    }

    # EMAIL CONSOL CONFIG
    EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'

    # CELERY CONFIG
    CELERY_TASK_TRACK_STARTED = True
    CELERY_TASK_TIME_LIMIT = 30 * 60
    CELERY_TIMEZONE = 'Asia/Yerevan'
    USE_TZ = False
    DJANGO_CELERY_BEAT_TZ_AWARE = False
