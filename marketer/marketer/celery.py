from __future__ import absolute_import
import os

import configurations
from django.conf import settings

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'marketer.settings.Dev')
os.environ.setdefault('DJANGO_CONFIGURATION', 'Dev')

configurations.setup()

app = Celery('marketer',
             broker=f'amqp://{os.getenv("RABBITMQ_DEFAULT_USER")}:{os.getenv("RABBITMQ_DEFAULT_PASS")}@rabbitmq:5672//')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


app.conf.task_default_queue = 'default'
app.conf.timezone = 'Asia/Yerevan'

app.conf.beat_schedule = {
    # delete after testing
    'show-time-every-3-minute': {
        'task': 'campaign.tasks.time_now',
        'schedule': 180.0,
    },
    'check-campaigns-schedule-time-every-5-minute': {
        'task': 'campaign.tasks.check_campaign_schedule_time',
        'schedule': crontab(minute=5),
    },
}
app.conf.timezone = 'UTC'
