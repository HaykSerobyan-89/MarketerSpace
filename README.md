<h1 align="center"> :zap: Marketer Space :zap:</h1>

![](https://img.shields.io/badge/Python-3.9.7-brightgreen)
![](https://img.shields.io/badge/Django-4.1.0-brightgreen)
![](https://img.shields.io/badge/Coverage-100%25-brightgreen)
![](https://img.shields.io/badge/docker%20build%20-automated-blue)

## *Task description*

The first user and the first organization should be created programmatically. It is the superuser. Users should be
connected to Organizations. The user can belong to only one organization, but the organization can have many users.
Users can not exist without organization. Users should have the following fields: first name, last name, profile
picture, organization, email, country, creation date, modification date, and other fields if required during
development. Only email is required. The organization should have the following fields: domain, and name. The main user
types are the superuser, user (without a specific role), and organization admin. Organizations can be created and
deleted only by superusers and updated by org admins and superusers. Organization information should be shown only to
organization users and superusers. Only a superuser can invite a user, and an org admin can invite a user to only its
organization. Organization admin can activate and deactivate, as well as delete the user of its org. Superusers can do
the same actions with all users. The superuser should have an opportunity to invite users (by email) to the organization
mentioning user type. When he invites the user, the user object should not be created unless he accepts the invitation.

The email subject should be:
Invitation from {first name} {last_name}: Marketer Space Body should be {first name} {last_name} ({email}) has invited
you to join {org name} organization:
Accept [invitaion link]

The invitation should be sent via email providing an invitation link. The link should expire if it is not accepted
within 1 day. The invitation data should be kept in DB. The auth should be done via JWT tokens.

## ***Run program with Docker***
```commandline
docker-compose up --build -d
```

## *Celery monitoring tool example in port 5555*
!["Celery monitoring tool example"](./marketer/marketer/media/celery_flower.png)

## *Coverage report*

```commandline
Name                                       Stmts   Miss  Cover
--------------------------------------------------------------
accounts/__init__.py                           0      0   100%
accounts/admin.py                              5      0   100%
accounts/apps.py                               4      0   100%
accounts/managers.py                          19      0   100%
accounts/migrations/0001_initial.py            7      0   100%
accounts/migrations/__init__.py                0      0   100%
accounts/mixins.py                            11      0   100%
accounts/models.py                            36      0   100%
accounts/permissions.py                       13      0   100%
accounts/serializers.py                       40      0   100%
accounts/tests/__init__.py                     0      0   100%
accounts/tests/test_accounts_viewsets.py     158      0   100%
accounts/tests/test_api_is_resolved.py        38      0   100%
accounts/tests/test_jwt_api.py                20      0   100%
accounts/tests/test_managers.py               41      0   100%
accounts/tests/test_serializers.py            23      0   100%
accounts/tests/test_settings.py               19      0   100%
accounts/urls.py                               7      0   100%
accounts/views.py                             92      0   100%
manage.py                                      7      0   100%
marketer/__init__.py                           0      0   100%
marketer/asgi.py                               4      0   100%
marketer/settings/Base.py                     28      0   100%
marketer/settings/Dev.py                      16      0   100%
marketer/settings/Prod.py                      4      0   100%
marketer/settings/__init__.py                  0      0   100%
marketer/urls.py                              17      0   100%
marketer/wsgi.py                               4      0   100%
--------------------------------------------------------------
TOTAL                                        613      0   100%
```