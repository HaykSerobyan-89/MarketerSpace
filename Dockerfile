FROM python:latest

LABEL Author="HaykSerobyan"

RUN pip install --upgrade pip

COPY ./requirements.txt .

RUN pip install -r requirements.txt

COPY ./marketer /marketer

WORKDIR /marketer

RUN flake8 --max-line-length=120 .